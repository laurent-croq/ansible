# GitLab CI template for Ansible

This project implements a Gitlab CI/CD template to provision your infrastructure and deploy 
your application with [Ansible](https://www.ansible.com/).

## Usage

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'to-be-continuous/ansible'
    ref: '5.1.0'
    file: '/templates/gitlab-ci-ansible.yml'
    
variables:
  ANSIBLE_DEFAULT_INVENTORY: "inventory.ini"
  ANSIBLE_DEFAULT_TAGS: "install"
  ANSIBLE_DEFAULT_EXTRA_ARGS: "-b"
  ANSIBLE_PROD_PLAYBOOK_FILE: "main.yml"
```

Don't forget to update the variables values ;-)

## Understand

This chapter introduces key notions and principle to understand how this template works.

### Managed deployment environments

This template implements Ansible-based deployments.

It allows you to manage automatic deployment & cleanup of standard predefined environments.
Each environment can be enabled/disabled by configuration.
If you're not satisfied with predefined environments and/or their associated Git workflow, you may implement you own environments and
workflow, by reusing/extending the base (hidden) jobs. This is advanced usage and will not be covered by this documentation.

The following chapters present the managed predefined environments and their associated Git workflow.

#### Review environments

The template supports **review** environments: those are dynamic and ephemeral environments to deploy your
_ongoing developments_ (a.k.a. _feature_ or _topic_ branches).

When enabled, it deploys the result from upstream build stages to a dedicated and temporary environment.
It is only active for non-production, non-integration branches.

It is a strict equivalent of GitLab's [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/) feature.

It also comes with a _cleanup_ job (accessible either from the _environments_ page, or from the pipeline view).

#### Integration environment

If you're using a Git Workflow with an integration branch (such as [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)),
the template supports an **integration** environment.

When enabled, it deploys the result from upstream build stages to a dedicated environment.
It is only active for your integration branch (`develop` by default).

#### Production environments

Lastly, the template supports 2 environments associated to your production branch (`master` or `main` by default):

* a **staging** environment (an iso-prod environment meant for testing and validation purpose),
* the **production** environment.

You're free to enable whichever or both, and you can also choose your deployment-to-production policy:

* **continuous deployment**: automatic deployment to production (when the upstream pipeline is successful),
* **continuous delivery**: deployment to production can be triggered manually (when the upstream pipeline is successful).

### Supported authentication methods

The Ansible template expects you to authenticate with SSH Keys (see [configuration reference](#configuration-reference) below).

### Deployment context variables

In order to manage the various deployment environments, this template provides a couple of **dynamic variables**
that you might use in your hook scripts (as env. variables) and Ansible playbooks (as [playbook variables](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_variables.html#defining-variables-at-runtime)):

* `environment_type`: the current deployment environment type (`review`, `integration`, `staging` or `production`)
* `environment_name`: a generated application name to use for the current deployment environment (ex: `myapp-review-fix-bug-12` or `myapp-staging`) - _details below_
* `ssh_public_key_file`: the path of the private key file (if an appropriate `$ANSIBLE_xxx_PUBLIC_KEY` variable is set)
* `ssh_private_key_file`: the path of the public key file (if an appropriate `$ANSIBLE_xxx_PRIVATE_KEY` variable is set)

#### Generated environment name

The `${environment_name}` variable is generated to designate each deployment environment with a unique and meaningful application name.
By construction, it is suitable for inclusion in DNS, URLs, Kubernetes labels...
It is built from:

* the application _base name_ (defaults to `$CI_PROJECT_NAME` but can be overridden globally and/or per deployment environment - _see configuration variables_)
* GitLab predefined `$CI_ENVIRONMENT_SLUG` variable ([sluggified](https://en.wikipedia.org/wiki/Clean_URL#Slug) name, truncated to 24 characters)

The `${environment_name}` variable is then evaluated as:

* `<app base name>` for the production environment
* `<app base name>-$CI_ENVIRONMENT_SLUG` for all other deployment environments
* :bulb: `${environment_name}` can also be overriden per environment with the appropriate configuration variable

Examples (with an application's base name `myapp`):

| `$environment_type` | Branch        | `$CI_ENVIRONMENT_SLUG`  | `$environment_name` |
|---------------------|---------------|-------------------------|---------------------|
| `review`            | `feat/blabla` | `review-feat-bla-xmuzs6`| `myapp-review-feat-bla-xmuzs6` |
| `integration`       | `develop`     | `integration`           | `myapp-integration` |
| `staging`           | `main`        | `staging`               | `myapp-staging` |
| `production`        | `main`        | `production`            | `myapp` |

### Deployment and cleanup scripts

Deployment with the Ansible playbook is performed by executing the environment playbook, using environment specific or default inventory, tags and extra args.

To activate environment cleanup, you need to set `ANSIBLE_xxx_CLEANUP_PLAYBOOK_FILE` and/or `ANSIBLE_xxx_CLEANUP_TAGS` in your environment.

Deployment jobs also support _optional_ **hook scripts** from your project, located in the `$ANSIBLE_SCRIPTS_DIR` directory
(root project dir by default, but may be overridden).

* `pre-ansible-playbook.sh` is executed **before** running `ansible-playbook` to perform specific environment pre-initialization such as generating a dynamic inventory,
* `post-ansible-playbook.sh` is executed **after** running `ansible-playbook` to perform specific environment post-initialization.

> :information_source: Your deployment (and cleanup) scripts have to be able to cope with various environments, each with different application names, exposed routes, settings, ...
> Part of this complexity can be handled by the lookup policies described above (ex: one script per env) and also by using available environment variables:
>
> 1. [deployment context variables](#deployment-context-variables) provided by the template:
>     * `${environment_type}`: the current environment type (`review`, `integration`, `staging` or `production`)
>     * `${environment_name}`: the application name to use for the current environment (ex: `myproject-review-fix-bug-12` or `myproject-staging`)
> 2. any [GitLab CI variable](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
> 3. any [custom variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project)
>     (ex: `${SECRET_TOKEN}` that you have set in your project CI/CD variables)

### Manage remote repositories authentication

Specifying an [Ansible requirements file](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#install-multiple-collections-with-a-requirements-file) (with `$ANSIBLE_REQUIREMENTS_FILE`), you may use Ansible roles from remote places (Ansible repository, Git repository, ...).
Accessing those repositories may require an authentication.

In such a case, you may configure the remote authentication(s) by providing a _variabilized_ `.netrc` file at the root of your Ansible project.
Here is an example:

```text
machine gitlab.com login gitlab-ci-token password ${CI_JOB_TOKEN}
machine ansible.acme.example login ${ACME_ANSIBLE_USER} password ${ACME_ANSIBLE_PASSWORD}
```

When such a file is detected, the Ansible template replaces each `${some_env}` pattern with actual environment values and installs it prior to executing `ansible-galaxy install`.

### Environments URL management

The AWS template supports two ways of providing your environments url:

* a **static way**: when the environments url can be determined in advance, probably because you're exposing your routes through a DNS you manage,
* a [**dynamic way**](https://docs.gitlab.com/ee/ci/environments/#set-dynamic-environment-urls-after-a-job-finishes): when the url cannot be known before the
  deployment job is executed.

The **static way** can be implemented simply by setting the appropriate configuration variable(s) depending on the environment (see environments configuration chapters):

* `$ANSIBLE_ENVIRONMENT_URL` to define a default url pattern for all your envs,
* `$ANSIBLE_REVIEW_ENVIRONMENT_URL`, `$ANSIBLE_INTEG_ENVIRONMENT_URL`, `$ANSIBLE_STAGING_ENVIRONMENT_URL` and `$ANSIBLE_PROD_ENVIRONMENT_URL` to override the default.

> :information_source: Each of those variables support a **late variable expansion mechanism** with the `%{somevar}` syntax, 
> allowing you to use any dynamically evaluated variables such as `${environment_name}`.
>
> Example:
>
> ```yaml
> variables:
>   ANSIBLE_BASE_APP_NAME: "wonderapp"
>   # global url for all environments
>   ANSIBLE_ENVIRONMENT_URL: "https://%{environment_name}.nonprod.acme.domain"
>   # override for prod (late expansion of $ANSIBLE_BASE_APP_NAME not needed here)
>   ANSIBLE_PROD_ENVIRONMENT_URL: "https://$ANSIBLE_BASE_APP_NAME.acme.domain"
>   # override for review (using separate resource paths)
>   ANSIBLE_REVIEW_ENVIRONMENT_URL: "https://wonderapp-review.nonprod.acme.domain/%{environment_name}"
> ```

To implement the **dynamic way**, your deployment script shall simply generate a `environment_url.txt` file in the working directory, containing only
the dynamically generated url. When detected by the template, it will use it as the newly deployed environment url.

### Deployment output variables

Each deployment job produces _output variables_ that are propagated to downstream jobs (using [dotenv artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv)):

* `$environment_type`: set to the type of environment (`review`, `integration`, `staging` or `production`),
* `$environment_name`: the application name (see below),
* `$environment_url`: set to the environment URL (whether determined statically or dynamically).

Those variables may be freely used in downstream jobs (for instance to run acceptance tests against the latest deployed environment).

You may also add and propagate your own custom variables, by pushing them to the `ansible.env` file in your [deployment script](#deployment-and-cleanup-scripts).

### Ansible commands overrides

Instead of creating hook scripts, you can also override and/or decorate the Ansible commands 
using predefined `.ansible-commands` template block, referenced by the [`!reference` directive](https://docs.gitlab.com/ee/ci/yaml/#reference-tags).

By default, the `.ansible-commands`, block is composed as below: 

```yaml
.ansible-commands:
  deploy: 
    - !reference [ .ansible-commands, default, deploy ]
  cleanup: 
    - !reference [ .ansible-commands, default, cleanup ]
```

You can override it for example in the following way:

```yaml
.ansible-commands:
  deploy: 
    - source sandbox.env
    - !reference [ .ansible-commands, default, deploy ]
    - echo "I'm executed after the Ansible deploy command"
```

You can use this mechanism to source to the current shell your own environmental variables. 

## Configuration reference

### Secrets management

Here are some advices about your **secrets** (variables marked with a :lock:):

1. Manage them as [project or group CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project):
    * [**masked**](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable) to prevent them from being inadvertently
      displayed in your job logs,
    * [**protected**](https://docs.gitlab.com/ee/ci/variables/#protected-cicd-variables) if you want to secure some secrets
      you don't want everyone in the project to have access to (for instance production secrets).
2. In case a secret contains [characters that prevent it from being masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable),
  simply define its value as the [Base64](https://en.wikipedia.org/wiki/Base64) encoded value prefixed with `@b64@`:
  it will then be possible to mask it and the template will automatically decode it prior to using it.
3. Don't forget to escape special characters (ex: `$` -> `$$`).

### Global configuration

This template can be configured with the following **environment variables**:

| Name                            | Description                                                                          | Default value                                  |
| ------------------------------- | ------------------------------------------------------------------------------------ | ---------------------------------------------- |
| `ANSIBLE_IMAGE`                 | The Docker image used to run Ansible. The image may contain your Ansible sources. <br/>:warning: **set the version required by your project** | `registry.hub.docker.com/cytopia/ansible:latest-tools` |
| `ANSIBLE_PROJECT_DIR`           | Ansible project root directory                                                       | `.`                                            |
| `ANSIBLE_BASE_APP_NAME`         | Base application name                  | `$CI_PROJECT_NAME` ([see GitLab doc](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)) |
| `ANSIBLE_ENVIRONMENT_URL`       | Default environments url _(only define for static environment URLs declaration)_<br/>_supports late variable expansion (ex: `https://%{environment_name}.acme.com`)_ | _none_ |
| :lock: `ANSIBLE_VAULT_PASSWORD` | The Ansible vault password used to decrypt vars.                                     | **has to be defined in gitlab secret if used** |
| :lock: `ANSIBLE_PRIVATE_KEY`    | The Ansible SSH private key to use in all stages (can be overridden per env)         | **has to be defined in gitlab secret if used** |
| `ANSIBLE_PUBLIC_KEY`            | The Ansible SSH public key associated to the private key to be use in all stages (can be overridden per env) | **has to be defined if used** |
| `ANSIBLE_DEFAULT_INVENTORY`     | The default inventory, if used                                                       | **has to be defined if used**                  |
| `ANSIBLE_DEFAULT_TAGS`          | The default tags, if used                                                            | **has to be defined if used**                  |
| `ANSIBLE_DEFAULT_EXTRA_ARGS`    | Optional default args to add to the ansible-playbook command line                    | **has to be defined if used**                  |
| `ANSIBLE_DEFAULT_ROLES_PATH`    | The default path where the roles should be installed                                 | `$CI_PROJECT_DIR/roles`                        |
| `ANSIBLE_FORCE_COLOR`           | Forces color on Ansible output                                                       | `true`                                         |
| `ANSIBLE_REQUIREMENTS_FILE`     | The file used to install roles with `ansible-galaxy install`                         | `requirements.yml`                             |
| `ANSIBLE_SCRIPTS_DIR`           | The Ansible scripts base directory (relative to `$ANSIBLE_PROJECT_DIR`)              | `.`                                            |
| `ANSIBLE_HOST_KEY_CHECKING`     | Enable or disable the SSH host key checking                                          | `false` (disabled)                             |

### Review environments configuration

Review environments are dynamic and ephemeral environments to deploy your _ongoing developments_ (a.k.a. _feature_ or _topic_ branches).

It is **disabled by default** and can be enabled by setting the `ANSIBLE_REVIEW_PLAYBOOK_FILE` variable (see below).

Here are variables supported to configure the integration environment:

| Name                            | Description                                        | Default value                                   |
| ------------------------------- | -------------------------------------------------- | ----------------------------------------------- |
| `ANSIBLE_REVIEW_APP_NAME`       | Application name for `review` env      | `"${ANSIBLE_BASE_APP_NAME}-${CI_ENVIRONMENT_SLUG}"` (ex: `myproject-review-fix-bug-12`) |
| `ANSIBLE_REVIEW_ENVIRONMENT_URL`| The review environments url _(only define for static environment URLs declaration and if different from default)_ | `$ANSIBLE_ENVIRONMENT_URL` |
| `ANSIBLE_REVIEW_INVENTORY`      | The inventory for `review` env                     | `$ANSIBLE_DEFAULT_INVENTORY`                    |
| `ANSIBLE_REVIEW_TAGS`           | The tags for `review` env                          | `$ANSIBLE_DEFAULT_TAGS`                         |
| `ANSIBLE_REVIEW_CLEANUP_TAGS`   | The tags to cleanup the `review` env               | **has to be defined if used**                   |
| `ANSIBLE_REVIEW_EXTRA_ARGS`     | The command line args for `review` env             | `$ANSIBLE_DEFAULT_EXTRA_ARGS`                   |
| `ANSIBLE_REVIEW_PLAYBOOK_FILE`  | The playbook filename for `review` env             | **has to be defined to enable the review env**  |
| `ANSIBLE_REVIEW_CLEANUP_PLAYBOOK_FILE`  | The playbook filename to cleanup `review` env             | `$ANSIBLE_REVIEW_PLAYBOOK_FILE`  |
| :lock: `ANSIBLE_REVIEW_PRIVATE_KEY`    | The SSH private key to be use in `review` env      | `$ANSIBLE_PRIVATE_KEY`                   |
| `ANSIBLE_REVIEW_PUBLIC_KEY`     | The SSH public key associated to the private key to be use in `review` env      | `$ANSIBLE_PUBLIC_KEY`                   |
| :lock: `ANSIBLE_REVIEW_VAULT_PASSWORD` | The Ansible vault password for `review` env        | `$ANSIBLE_VAULT_PASSWORD`                 |

### Integration environment configuration

The integration environment is the environment associated to your integration branch (`develop` by default).

It is **disabled by default** and can be enabled by setting the `ANSIBLE_INTEG_PLAYBOOK_FILE` variable (see below).

Here are variables supported to configure the integration environment:

| Name                            | Description                                        | Default value                                   |
| ------------------------------- | -------------------------------------------------- | ----------------------------------------------- |
| `ANSIBLE_INTEG_APP_NAME`        | Application name for `integration` env | `${ANSIBLE_BASE_APP_NAME}-integration` |
| `ANSIBLE_INTEG_ENVIRONMENT_URL` | The integration environment url _(only define for static environment URLs declaration and if different from default)_ | `$ANSIBLE_ENVIRONMENT_URL` |
| `ANSIBLE_INTEG_INVENTORY`       | The inventory for `integration` env                | `$ANSIBLE_DEFAULT_INVENTORY`                    |
| `ANSIBLE_INTEG_TAGS`            | The tags for `integration` env                     | `$ANSIBLE_DEFAULT_TAGS`                         |
| `ANSIBLE_INTEG_CLEANUP_TAGS`    | The tags to cleanup the `integration` env          | **has to be defined if used**                   |
| `ANSIBLE_INTEG_EXTRA_ARGS`      | The command line args  for `integration` env       | `$ANSIBLE_DEFAULT_EXTRA_ARGS`                   |
| `ANSIBLE_INTEG_PLAYBOOK_FILE`   | The playbook filename for `integration` env        | **has to be defined to enable the integration env** |
| `ANSIBLE_INTEG_CLEANUP_PLAYBOOK_FILE` | The playbook filename to cleanup `integration` env     | `$ANSIBLE_INTEG_PLAYBOOK_FILE` |
| :lock: `ANSIBLE_INTEG_PRIVATE_KEY`    | The SSH private key to be use in `integration` env     | `$ANSIBLE_PRIVATE_KEY`                  |
| `ANSIBLE_INTEG_PUBLIC_KEY`      | The SSH public key associated to the private key to be use in `integration` env  | `$ANSIBLE_PUBLIC_KEY`                   |
| :lock: `ANSIBLE_INTEG_VAULT_PASSWORD` | The Ansible vault password for `integration` env       | `$ANSIBLE_VAULT_PASSWORD`                |

### Staging environment configuration

The staging environment is an iso-prod environment meant for testing and validation purpose associated to your production branch (`main` or `master` by default).

It is **disabled by default** and can be enabled by setting the `ANSIBLE_STAGING_PLAYBOOK_FILE` variable (see below).

Here are variables supported to configure the staging environment:

| Name                            | Description                                        | Default value                                   |
| ------------------------------- | -------------------------------------------------- | ----------------------------------------------- |
| `ANSIBLE_STAGING_APP_NAME`      | Application name for `staging` env     | `${ANSIBLE_BASE_APP_NAME}-staging` |
| `ANSIBLE_STAGING_ENVIRONMENT_URL`| The staging environment url _(only define for static environment URLs declaration and if different from default)_ | `$ANSIBLE_ENVIRONMENT_URL` |
| `ANSIBLE_STAGING_INVENTORY`     | The inventory for `staging` env                    | `$ANSIBLE_DEFAULT_INVENTORY`                    |
| `ANSIBLE_STAGING_TAGS`          | The tags for `staging` env                         | `$ANSIBLE_DEFAULT_TAGS`                         |
| `ANSIBLE_STAGING_CLEANUP_TAGS`  | The tags to cleanup the `staging` env              | **has to be defined if used**                   |
| `ANSIBLE_STAGING_EXTRA_ARGS`    | The command line args  for `staging` env           | `$ANSIBLE_DEFAULT_EXTRA_ARGS`                   |
| `ANSIBLE_STAGING_PLAYBOOK_FILE` | The playbook filename for `staging` env            | **has to be defined to enable the staging env** |
| `ANSIBLE_STAGING_CLEANUP_PLAYBOOK_FILE` | The playbook filename to cleanup `staging` env            | `$ANSIBLE_STAGING_PLAYBOOK_FILE` |
| :lock: `ANSIBLE_STAGING_PRIVATE_KEY`    | The SSH private key to be use in `staging` env     | `$ANSIBLE_PRIVATE_KEY`                  |
| `ANSIBLE_STAGING_PUBLIC_KEY`     | The SSH public key associated to the private key to be use in `staging` env      | `$ANSIBLE_PUBLIC_KEY`                   |
| :lock: `ANSIBLE_STAGING_VAULT_PASSWORD` | The Ansible vault password for `staging` env       | `$ANSIBLE_VAULT_PASSWORD`                |

### Production environment configuration

| Name                            | Description                                        | Default value                                             |
| ------------------------------- | -------------------------------------------------- | --------------------------------------------------------- |
| `ANSIBLE_PROD_APP_NAME`         | Application name for `production` env  | `$ANSIBLE_BASE_APP_NAME` |
| `ANSIBLE_PROD_ENVIRONMENT_URL`  | The production environment url _(only define for static environment URLs declaration and if different from default)_ | `$ANSIBLE_ENVIRONMENT_URL` |
| `ANSIBLE_PROD_DEPLOY_STRATEGY`  | Defines the deployment to production strategy. One of `manual` (i.e. _one-click_) or `auto`. | `manual` |
| `ANSIBLE_PROD_INVENTORY`        | The inventory for `production` env                 | `$ANSIBLE_DEFAULT_INVENTORY`                              |
| `ANSIBLE_PROD_TAGS`             | The tags for `production` env                      | `$ANSIBLE_DEFAULT_TAGS`                                   |
| `ANSIBLE_PROD_EXTRA_ARGS`       | The command line args  for `production` env        | `$ANSIBLE_DEFAULT_EXTRA_ARGS`                             |
| `ANSIBLE_PROD_PLAYBOOK_FILE`    | The playbook filename for `production` env         | **has to be defined**                                     |
| :lock: `ANSIBLE_PROD_PRIVATE_KEY`      | The SSH private key to be use in `production` env  | `$ANSIBLE_PRIVATE_KEY`                             |
| `ANSIBLE_PROD_PUBLIC_KEY`     | The SSH public key associated to the private key to be use in `production` env      | `$ANSIBLE_PUBLIC_KEY`                   |
| :lock: `ANSIBLE_PROD_VAULT_PASSWORD`   | The Ansible vault password for `production` env    | `$ANSIBLE_VAULT_PASSWORD`                           |

Variables that you want to define or override default have to be defined as a project or group CI/CD variable or globally in your `.gitlab-ci.yml` file.

### Ansible lint job

The Ansible template enables [Ansible Lint](https://docs.ansible.com/ansible-lint/) analysis of your Ansible scripts.

This job is mapped to the `test` stage and is **active** by default.

Here are its parameters:

| Name                    | Description                               | Default value                 |
| ----------------------- | ----------------------------------------- | ----------------------------- |
| `ANSIBLE_LINT_IMAGE`    | The Docker image used to run Ansible Lint | `registry.hub.docker.com/haxorof/ansible-lint:latest` |
| `ANSIBLE_LINT_DISABLED` | Set to `true` to disable the `ansible lint` analysis  | _none_ (enabled) |

In addition to a textual report in the console, this job produces the following report, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `$ANSIBLE_PROJECT_DIR/reports/ansible-lint-*.codeclimate.json` | [codeclimate format](https://ansible-lint.readthedocs.io/usage/#id2) | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportscodequality) |

## Examples

* Review environment enabled,
* **Continuous deployment** to production.

Examples are available in the [sample section](/to-be-continuous/samples?utf8=%E2%9C%93&filter=ansible)

### Deploy and configure your infrastructure components

The `gitlab-ci` in this example, will show use how to configure the template to deploy your infrastructure and configure your components, from your ansible playbook repository. If you don't use an inventory, for example if you're building an openstack infrastruture, you can remove the `*_INVENTORY` variables.

```yaml
include:
  - project: 'to-be-continuous/ansible'
    ref: '5.1.0'
    file: '/templates/gitlab-ci-ansible.yml'
    
variables:
  # Default variables
  # In this example, I consider that you have one inventory per platform 
  # and only one playbook file for all environments
  ANSIBLE_DEFAULT_EXTRA_ARGS: "-b" 
  ANSIBLE_DEFAULT_TAGS: "install"
  
  # Review
  ANSIBLE_REVIEW_INVENTORY: "review.ini"
  ANSIBLE_REVIEW_CLEANUP_TAGS: "destroy"
  ANSIBLE_REVIEW_PLAYBOOK_FILE: "$ANSIBLE_PROD_PLAYBOOK_FILE"
  
  # Staging
  ANSIBLE_STAGING_INVENTORY: "staging.ini"
  ANSIBLE_STAGING_PLAYBOOK_FILE: "$ANSIBLE_PROD_PLAYBOOK_FILE"
  
  # Production
  ANSIBLE_PROD_INVENTORY: "production.ini"
  ANSIBLE_PROD_PLAYBOOK_FILE: "main.yml" 

stages:
  - build
  - deploy
  - production
```

Don't forget to add `$ANSIBLE_VAULT_PASSWORD` & `$ANSIBLE_PRIVATE_KEY` in Gitlab secret variables.

### Deploy your application

The `gitlab-ci` in this example, will show use how to configure the template to deploy your application, from your app repository (different from the ansible repo).

#### Ansible playbook repository

To use this template from a gitlab project that doesn't contains you ansible playbook (for example, your code repository), you need to package your ansible sources. Add this `Dockerfile` in your ansible project and edit the `ENV` variables to match your needs:

```dockerfile
FROM williamyeh/ansible:alpine3

# Add here every package you will need to build libraries
# These packages will be removed after dependancies install 
ENV BUILD_PACKAGES="git gcc libc-dev linux-headers python2-dev"
# Packages to include in the image
ENV PACKAGES="ca-certificates bash openssh curl"
# Python dependancies to include in the image
ENV PIP_PACKAGES="openstacksdk"
# Your ansible source folder
ENV ANSIBLE_SRC_DIR="."

# Create ansible group and user
RUN addgroup -S ansible && adduser -S ansible -G ansible

RUN apk add --no-cache --virtual .build-deps $BUILD_PACKAGES \
    && apk add --no-cache $PACKAGES \
    && pip install --no-cache-dir --upgrade pip \
    && pip install --no-cache-dir --upgrade $PIP_PACKAGES \
    && apk del --no-cache --purge .build-deps \
    && rm -rf /var/cache/apk/*

# Tell docker that all future commands should run as ansible user
USER ansible

COPY $ANSIBLE_SRC_DIR   /home/ansible/playbook

WORKDIR /home/ansible/playbook

ENTRYPOINT [""]
CMD ["ansible-playbook","--version"]
```

In the `.gitlab-ci.yml` add the docker template to build your image:

```yaml
include:
  - project: 'to-be-continuous/ansible'
    ref: '5.1.0'
    file: '/templates/gitlab-ci-ansible.yml'
  - project: 'to-be-continuous/docker'
    ref: '5.1.0'
    file: '/templates/gitlab-ci-docker.yml'
    
variables:
  # Default variables
  # In this example, I consider that you have one inventory per platform 
  # and only one playbook file for all environments
  ANSIBLE_DEFAULT_EXTRA_ARGS: "-b"
  ANSIBLE_DEFAULT_TAGS: "install"
  
  # Review
  ANSIBLE_REVIEW_INVENTORY: "review.ini"
  ANSIBLE_REVIEW_CLEANUP_TAGS: "destroy"
  ANSIBLE_REVIEW_PLAYBOOK_FILE: "$ANSIBLE_PROD_PLAYBOOK_FILE"

  # Staging
  ANSIBLE_STAGING_INVENTORY: "staging.ini"
  ANSIBLE_STAGING_PLAYBOOK_FILE: "$ANSIBLE_PROD_PLAYBOOK_FILE"

  # Production
  ANSIBLE_PROD_INVENTORY: "production.ini"
  ANSIBLE_PROD_PLAYBOOK_FILE: "main.yml"  
  
stages:
  - build
  - package-build
  - package-test
  - deploy
  - publish
  - production
```

By default, the docker template will push the image in the Gitlab docker registry. Please visit, the docker template page to get more information on [Docker Template](https://gitlab.com//to-be-continuous/docker)

#### Application repository

Add this `.gitlab-ci.yml` in the repository of your application project (you will need to add your build & package configuration in this ci)

```yaml
include:
  - project: 'to-be-continuous/ansible'
    ref: '5.1.0'
    file: '/templates/gitlab-ci-ansible.yml'
    
variables:
  # Default variables
  # In this example, I consider that you have one inventory per platform 
  # and only one playbook file for all environments
  ANSIBLE_DEFAULT_EXTRA_ARGS: "-b" 
  ANSIBLE_DEFAULT_TAGS: "deploy"
  # Ansible lint has been launched on the ansible pipeline
  ANSIBLE_LINT_DISABLED: "true"
  # The ansible image with your playbook
  ANSIBLE_IMAGE: "registry.example/ender/ansible-playbook:master"
  
  # Review
  ANSIBLE_REVIEW_INVENTORY: "/home/ansible/playbook/review.ini"
  ANSIBLE_REVIEW_CLEANUP_TAGS: "undeploy"
  ANSIBLE_REVIEW_PLAYBOOK_FILE: "$ANSIBLE_PROD_PLAYBOOK_FILE"
  
  # Staging
  ANSIBLE_STAGING_INVENTORY: "/home/ansible/playbook/staging.ini"
  ANSIBLE_STAGING_PLAYBOOK_FILE: "$ANSIBLE_PROD_PLAYBOOK_FILE"
  
  # Production
  ANSIBLE_PROD_INVENTORY: "/home/ansible/playbook/production.ini"
  ANSIBLE_PROD_PLAYBOOK_FILE: "/home/ansible/playbook/main.yml" 
  
stages:
  - build
  - deploy
  - production
```

## Variants

The default Ansible template is designed to work on untagged runners, without any proxy configuration using Docker images
from the internet and using Default Trusted Certificate Authorities.

Nevertheless there are template variants available to cover specific cases.

### Vault variant

This variant allows delegating your secrets management to a [Vault](https://www.vaultproject.io/) server.

#### Configuration

In order to be able to communicate with the Vault server, the variant requires the additional configuration parameters:

| Name              | description                            | default value     |
| ----------------- | -------------------------------------- | ----------------- |
| `TBC_VAULT_IMAGE` | The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use (can be overridden) | `$CI_REGISTRY/to-be-continuous/tools/vault-secrets-provider:master` |
| `VAULT_BASE_URL`  | The Vault server base API url          | _none_ |
| `VAULT_OIDC_AUD`  | The `aud` claim for the JWT | `$CI_SERVER_URL` |
| :lock: `VAULT_ROLE_ID`   | The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID | **must be defined** |
| :lock: `VAULT_SECRET_ID` | The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID | **must be defined** |

#### Usage

Then you may retrieve any of your secret(s) from Vault using the following syntax:

```text
@url@http://vault-secrets-provider/api/secrets/{secret_path}?field={field}
```

With:

| Name                             | description                            |
| -------------------------------- | -------------------------------------- |
| `secret_path` (_path parameter_) | this is your secret location in the Vault server |
| `field` (_query parameter_)      | parameter to access a single basic field from the secret JSON payload |

#### Example

```yaml
include:
  # main template
  - project: 'to-be-continuous/ansible'
    ref: '5.1.0'
    file: '/templates/gitlab-ci-ansible.yml'
  # Vault variant
  - project: 'to-be-continuous/ansible'
    ref: '5.1.0'
    file: '/templates/gitlab-ci-ansible-vault.yml'

variables:
    # audience claim for JWT
    VAULT_OIDC_AUD: "https://vault.acme.host"
    # Secrets managed by Vault
    ANSIBLE_VAULT_PASSWORD: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-infra/ansible?field=vault.password"
    ANSIBLE_PRIVATE_KEY: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-infra/ansible?field=priv_key"
    VAULT_BASE_URL: "https://vault.acme.host/v1"
    # $VAULT_ROLE_ID and $VAULT_SECRET_ID defined as a secret CI/CD variable
```
